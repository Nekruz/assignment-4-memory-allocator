#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"


void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );


static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

bool valid_address(void* addr) {
  return addr == MAP_FAILED || addr == NULL;
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  struct region region;
  block_size newSize = size_from_capacity((block_capacity) {query});
  size_t actual_size = region_actual_size(newSize.bytes);
  newSize = (block_size) {actual_size};
  void* new_address = map_pages(addr, actual_size, MAP_FIXED_NOREPLACE | MAP_FIXED);
  if (valid_address(new_address)) {
    new_address = map_pages(addr, actual_size, 0);
    if (valid_address(new_address)){
      return REGION_INVALID;
    }
  }
  region = (struct region) {new_address, actual_size, false};
  if (new_address == addr) {
    region.extends = true;
  }
  block_init(region.addr, newSize, NULL);
  return region;
  /*  ??? */
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  /* 
  
  
  Block dividing implementation
  
  
  */
  if (!block_splittable(block, query)) {
    return false;
  }
  //computing second part
  void *second_part = (void*)((uint8_t*) block+query+offsetof(struct block_header, contents));
  block_capacity newcapacity;
  newcapacity.bytes = size_from_capacity(block->capacity).bytes - query - 2 * offsetof(struct block_header, contents);
  block_size size_of_second_part = size_from_capacity(newcapacity);
  //init block
  block_init(second_part, size_of_second_part, block->next);
  //Adjusting parameters of first part
  newcapacity.bytes = query;
  block->next = second_part;
  block->capacity = newcapacity;
  return true;
  /*  ??? */
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (block->next == NULL || !mergeable(block, block->next)) {
    return false;
  }
  struct block_header* next_block = block->next;
  block->capacity.bytes += size_from_capacity(next_block->capacity).bytes;
  block->next = next_block->next;
  //next_block = NULL;
  return true;
  /*  ??? */
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
  struct block_header* cur = block;
  while (1) {
    while (!cur->is_free && cur->next != NULL) {
      cur = cur->next;
    }
    while (try_merge_with_next(cur));
    if (block_is_big_enough(sz, cur)) {
      break;
    }
    if (cur->next != NULL) {
      cur = cur->next;
    } else {
      break;
    }
  }
  struct block_search_result ans;
  ans.block = cur;
  if (block_is_big_enough(sz, cur) && cur->is_free) {
    ans.type = BSR_FOUND_GOOD_BLOCK;
  } else {
    ans.type = BSR_REACHED_END_NOT_FOUND;
  }
  return ans;
  /*??? */
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result temp = find_good_or_last(block, query);
  if (temp.type == BSR_FOUND_GOOD_BLOCK) {
    split_if_too_big(temp.block, query >= BLOCK_MIN_CAPACITY ? query : BLOCK_MIN_CAPACITY);
    temp.block->is_free = false;
  }
  return temp;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  void* next = block_after(last);
  struct region new_region = alloc_region(next, query);
  if (region_is_invalid(&new_region)) {
    return NULL;
  }
  block_init(new_region.addr, (block_size) {new_region.size}, NULL);
  last->next = new_region.addr;
  return try_merge_with_next(last) ? last : last->next;
  /*  ??? */
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  /*  
  
    Implementation of memalloc 1st approach 
    
  */
  struct block_header* cur = heap_start;
  /*
  
  malloc 2nd approach
  
  */
  struct block_search_result temp = try_memalloc_existing(query, cur);
  if (temp.type == BSR_REACHED_END_NOT_FOUND) {
    struct block_header* result = grow_heap(temp.block, query);
    if (result == NULL) {
      return NULL;
    }
    temp = try_memalloc_existing(query, cur);
  }
  if (temp.type == BSR_CORRUPTED) return NULL;
  cur = temp.block;
  return cur;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while (true) {
    bool result = try_merge_with_next(header);
    if (!result) break;
  }
  /*  ??? */
}
