#include "mem.h"
#include "mem_internals.h"

int test1() {
    printf("Simple malloc test\n");
    void *heap = heap_init(0);
    void *ptr1 = _malloc(REGION_MIN_SIZE / 2);
    if (ptr1 == NULL) {
        fprintf(stderr, "Something went wrong in allocation half of the available size\n");
        return 0;
    }
    printf("Allocation of half of available size successful:\n");
    // At the same time test too small mallocs
    void *ptr2 = _malloc(1);
    if (ptr2 == NULL) {
        _free(ptr1);
        fprintf(stderr, "Something went wrong in allocating too few bytes\n");
        return 0;
    }
    printf("Allocation of 1 byte successful\n");
    printf("Simple malloc test successful, see heap for reference:\n");
    debug_heap(stdout, heap);
    return 1;
}

int test2() {
    printf("Simple free test\n");
    void *heap = heap_init(0);
    void *ptr1 = _malloc(REGION_MIN_SIZE / 3);
    if (ptr1 == NULL) {
        fprintf(stderr, "Something went wrong in allocation half of the available size\n");
        return 0;
    }
    void *ptr2 = _malloc(100);
    if (ptr2 == NULL) {
        _free(ptr1);
        fprintf(stderr, "Something went wrong in allocating 100 bytes\n");
        return 0;
    }
    _free(ptr1);
    printf("Successful memory free of one block see heap for reference:\n");
    debug_heap(stdout, heap);
    return 1;
}

int test3() {
    printf("Free all test\n");
    void *heap = heap_init(0);
    void *ptr1 = _malloc(REGION_MIN_SIZE / 3);
    if (ptr1 == NULL) {
        fprintf(stderr, "Something went wrong in allocation one third of the available size");
        return 0;
    }
    void *ptr2 = _malloc(100);
    if (ptr2 == NULL) {
        _free(ptr1);
        fprintf(stderr, "Something went wrong in allocating 100 bytes");
        return 0;
    }
    printf("Before first free\n");
    debug_heap(stdout, heap);
    _free(ptr1);
    printf("After first free\n");
    debug_heap(stdout, heap);
    _free(ptr2);
    printf("Successfull memory free of all allocated blocks:\n");
    printf("Final state\n");
    debug_heap(stdout, heap);
    return 1;
}

int test4() {
    printf("First overflow test\n");
    void *heap = heap_init(0);
    printf("Before first allocation\n");
    debug_heap(stdout, heap);
    void *ptr1 = _malloc(REGION_MIN_SIZE / 3);
    if (ptr1 == NULL) {
        fprintf(stderr, "Something went wrong in allocation one third of the available size");
        return 0;
    }
    printf("Successfull allocation of first block:\n");
    printf("Before second allocation\n");
    void *ptr2 = _malloc(4 * REGION_MIN_SIZE / 3);
    if (ptr2 == NULL) {
        _free(ptr1);
        fprintf(stderr, "Something went wrong in allocating 100 bytes");
        return 0;
    }
    printf("Successful extension of heap and allocation and heap growth:\n");
    printf("Before first free and after first heap growth\n");
    debug_heap(stdout, heap);
    _free(ptr1);
    printf("After first free\n");
    debug_heap(stdout, heap);
    _free(ptr2);
    printf("Successful extension of heap and allocation:\n");
    printf("Final state\n");
    debug_heap(stdout, heap);
    return 1;
}

int test5() {
    printf("Many mallocs many frees test\n");
    void *heap = heap_init(0);
    printf("Before first allocation\n");
    debug_heap(stdout, heap);
    void* ptrs[10];
    size_t ssize = REGION_MIN_SIZE / 5;
    for (int i = 0; i < 10; i++) {
        printf("Before %ist allocation\n", i);
        debug_heap(stdout, heap);
        ptrs[i] = _malloc(ssize);
    }
    printf("Successful allocation of 10 blocks of same size:\n");
    for (int i = 9; i >= 0; i--) {
        printf("Before %ist free\n", i);
        debug_heap(stdout, heap);
        _free(ptrs[i]);
    }
    printf("Successful memory free of all allocated blocks, see heap for reference:\n");
    debug_heap(stdout, heap);
    return 1;
}

void test() {
    if (test1() && test2() && test3() && test4() && test5()) {
        printf("all test passed\n");
    } else {
        printf("Something went wrong\n");
    }
}